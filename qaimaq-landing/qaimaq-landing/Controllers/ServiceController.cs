﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using qaimaq_landing.Models;

namespace qaimaq_landing.Controllers
{
    public class ServiceController : Controller
    {
        public IActionResult Service1()
        {
            ViewData["Message"] = "Your service page.";

            return View();
        }

        public IActionResult Service2()
        {
            ViewData["Message"] = "Your service page.";

            return View();
        }

        public IActionResult Service3()
        {
            ViewData["Message"] = "Your service page.";

            return View();
        }

        public IActionResult Service4()
        {
            ViewData["Message"] = "Your service page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
